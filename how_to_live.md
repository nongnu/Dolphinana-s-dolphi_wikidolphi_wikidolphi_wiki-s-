# How To 

(TODO)


- Support free software, free culture, public domain, simplicity, information freedom, anarchism, pacifism.
- Don't work.
- Reject violence.
- Spend minimal amount of money as possible.
- Help people.
- Destroy corporations.
- Be open to other people, don't keep everything to yourself.
- Don't use malicious social media.
- Live in a small town or woods, rather than in the city.
- 

(TODO)



## How to get a computing device

First off, do not buy a new PC/Mac from a big store. They contain a lot of malicious features that won't go away even if you install a more ethical operating system such as GNU/Linux or BSD. Those malicious feature could be: bloat, proprietary firmware, spyware in CPU, non-user servicable parts and many more. Also these computers are expensive and make you a slave. 
Perhaps, you probably won't have to pay for a computer, you can probably get one for free from a neighbor. {I got an iMac from 2007 for free from my neighbor. It can run GNU/Linux :) I gave that computer away to someone.  ~dolphinana} You can maybe find computers at scrapyard or recycling centers, some schools and workplaces might have old computers that they would give you for free. If you're lucky, you might find a computer that are [libre-](libreboot.md)/[corebootable](coreboot.md).
If that doesn't work, you can find some cheap computers at eBay or any similar site. You can also buy computers that have libreboot preinstalled, they are a bit expensive though, it's up to you if you wanna buy it or not.
Some devices that are not really consider to be computers such as cellphone, camera, game consoles, routers etcetc.. those could also be useful. {I found a Nintendo DS that someone was selling outside for 5€, I bought it, but I still don't have the charger for it so I don't know if it works or not.  ~dolphinana}

Also, support libre hardware developer, buy one from them. 
