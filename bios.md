# BIOS

(TODO)

BIOS (also known as Basic Input Output System) is a software in your computer that runs when your computer starts, it will initialize some hardware and then loads the bootloader. It also provides some functionality for the operating system.

Many computers have their own BIOS that are proprietary and malicious. Thankfully, there are coreboot and libreboot though replacing proprietary BIOS with the free one can be difficult and risky. Some proprietary BIOS won't let you replace it with internal flashing, might have to disassemble the computer and flash it externally. If the proprietary BIOS don't restrict flashing, you can simply install free BIOS by running flashrom on the computer you want to flash.

Many modern computers have a thing called Intel Boot Guard, which is a malicious feature that checks if the BIOS is signed by the vendor, if not, the computer will not run the unsigned BIOS. It is there in the name of security, obviously.
