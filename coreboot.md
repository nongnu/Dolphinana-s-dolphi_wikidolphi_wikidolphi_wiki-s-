# coreboot

Coreboot is an "open source" firmware that can replace proprietary BIOS on your PC. It initializes hardware and then jumps to a payload such as Grub or seaBIOS. Almost every chromebooks have coreboot installed. [Libreboot](libreboot.md) is a distribution of coreboot that are easier to install for less technical people.

A free software enthusiast and vlogger named tripcodeq7 makes videos on installing coreboot on various Thinkpads. 
