# Libreboot

Libreboot is a project that provides free BIOS for computer. It is a distribution of coreboot and provides a build system that would build bios rom. When powering on Librebooted computer, it would first do some low level hardware initialization and then when it's done, it will jump to a payload such as Grub bootloader. As of November 2022, libreboot is compatible with an impressive number, 36 hardware supported! It's mostly Thinkpads, but also Chromebooks, Macs, Asus motherboards and more. Unfortunately, this number is nothing compared to thousands of different laptops and motherboards that exists in this world. 

On some hardware, Libreboot can simply be installed just by running flashrom with few parameters. However, Libreboot on some hardware must be installed externally, meaning you'd have do disassemble the computer, locate the bios chip and put the programmer on the bios chip, the programmer would be plugged into a single-board-computer like BeagleBone Black or Raspberry PI, run flashrom with some parameters (but first, backup the original BIOS!!!) then plug the charger into the laptop (no battery is needed) and try power it on. Verify that Libreboot was properly installed. Congratulations, your computer is even more libre now! Though keep in mind, there'll still be some non-free firmware like EC firmware and HDD/SSD firmware. Your computer will not magically be 100% libre with libreboot. But on the bright side, a librebooted computer is one of the most libre computer you could have, aside from certain libre hardware that are somewhat rare and expensive.

There are some online shops that sell computers with Libreboot preinstalled such as minifree.org, store.vikings.net, technoethical.com and more {I know one more store, but I forgot its name.   ~dolphinana}. 

In November 2022, Libreboot stopped following Free Software Distribution Guidelines due to being shitty, this means that newer versions of Libreboot is no longer FSF approved xD Haha FSF you got pwn'd!!! Nowadays, Libreboot includes proprietary firmware for devices that need it. Although we despise all kinds of proprietary works, banning non-free firmware will discourage people from using free software if there is no libre firmware for their hardware. This could hurt software freedom in the long term. Make a long term decision, not bullshit.




{I don't have a Librebooted computer yet :(   ~dolphinana}
