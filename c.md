# C Programming Language

C is a low-level compiled programming language that can be considered a step up from [assembly](assembly.md). It is pretty fast and programs written in C can be compiled to many different platforms, making it portable if done correctly.

There are many different C compilers such as [GCC](gcc.md), [clang](clang), [tcc](tcc.md) and many more. 


There is a C tutorial at LRS wiki! ( https://www.tastyfish.cz/lrs/c_tutorial.html )
