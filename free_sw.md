# Free Software

(TODO)

Free software is a type of software that respects its users' freedom. For a software to be free, it has to grant its users' four essential freedom:

- Freedom 0 -- Run the software whenever you wish and for any purpose.
- Freedom 1 -- Study and modify the program. Access to source code is a precondition.
- Freedom 2 -- Redistribute copies of the unmodified program, so you can help your neighbors.
- Freedom 3 -- Redistribute copies of your modification to that program, so the society can benefit from your derivative work.

If the software grants all of these freedoms, then it's free software, respecting its users' freedom. But if one of these freedoms are missing, the software is proprietary and abuses its users' and is considered unethical from our point of view.

It's important to stress that the definition of free software we use, is NOT about software that can be obtained gratis. It's about freedom!
