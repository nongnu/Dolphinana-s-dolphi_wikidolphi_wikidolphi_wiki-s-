# OpeniBoot

(This might get merged with iphone.md or idroidproject.md later)
(TODO)

OpeniBoot is a forgotten bootloader for older Apple iThings that can run unsigned code, unlike iBoot. It is pretty buggy and the latest version is 0.3, it is made for booting Android on iThing, but it can also boot GNU/Linux.

OpeniBoot prior to version 0.3 have a menu with nice graphics but are less flexible. OpeniBoot 0.3 have a grub like menu and requires menu.lst file.

OpeniBoot supports:

- iPhone1,1 (iPhone)
- iPhone1,2 (iPhone 3G)
- iPhone1,3 (iPhone 3GS) -partially
- iPhone1,4 (iPhone 4) -partially
- iPod1,1 (iPod Touch)
- iPad1,1 (iPad) -partially?

{The info above is slightly incorrect, I'll fix it soon.  ~dolphinana}


## Cheatsheet


### OpeniBoot 0.1

!zImage                                  - Loads a file named zImage from your computer into the phone.
kernel                                   - Loads the kernel into memory, use !file command first.
ramdisk                                  - Loads the ramdisk into memory, use !file command first.
boot (arguments)                         - Boot the kernel, pass arguments such as "root=/dev/ram0".
reboot                                   - Reboot the phone.
install                                  - Install OpeniBoot onto the phone.
vibrator_loop                            - Run this command to vibrate your gud ol' pussy xD
fs_ls (partition num) (directory)        - List directories in filesystem.
fs_cat (partition num) (file)            - Concatenate a file.

TODO: Moar
