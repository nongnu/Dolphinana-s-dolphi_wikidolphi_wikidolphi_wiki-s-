# Spotify

*Any song I listen to, I can share a copy too.*

Spotify is a shitty music streaming dis-service that successfully ruined the music world with its shitty [anti-sharing](drm.md) technology, as a way to make people more anti-social and to benefit capitalism. Normies use it, we don't.

Spotify, just like many other drm-based dis-services, gives people an illusion of sharing with other people. When really, they're really just telling other people to listen to a certain music on Spotify. This is [network effect](network_effect.md).

Before Spotify existed, most people would get pirate copies of music since it was too expensive to buy "legitimate" copies of musical albums back then. Some retarded guy in Sweden wanted to fight piracy and started Spotify, unfortunately, it was successful and now almost everyone is using it.

Spotify is known to collect user data and use it manipulate its users. Pretty generic corporate stuff.
